<?php

namespace Drupal\force_file_delete\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class ForceFileDeleteForm.
 */
class ForceFileDeleteForm extends FormBase {

  /**
   * EntityTypeManager definition.
   *
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityInterface|null
   */
  private $loadedEntityReferenceFile;

  private $unmanagedFileUri;

  /**
   * Constructs a new ForceFileDeleteForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   */
  public function __construct(
    EntityTypeManager $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'force_file_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['managed_file_to_delete'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'file',
      '#title' => $this->t('Managed File to Delete'),
      '#description' => $this->t('Managed file to delete.'),
      '#weight' => '0',
    ];
    $form['uri_of_file_to_delete'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public Uri Path of File to Delete'),
      '#description' => $this->t('Public uri path of file to delete For example: public://legacy_files/document1.pdf'),
      '#maxlength' => 500,
      '#size' => 64,
      '#weight' => '1',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => '2'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_values = $form_state->getValues();

    if (!empty($form_values['managed_file_to_delete'])) {
      if (!$this->loadedEntityReferenceFile = $this->entityTypeManager->getStorage('file')->load($form_values['managed_file_to_delete'])) {
        $form_state->setErrorByName('managed_file_to_delete', 'Invalid file');
      }
    }

    if (!empty($form_values['uri_of_file_to_delete'])) {
      $uri = $form_values['uri_of_file_to_delete'];
      if (strpos($uri, 'public://') !== 0) {
        $form_state->setErrorByName('managed_file_to_delete', 'Invalid unmanaged file path. Please provide a public file path such as public://file.png');
      }

      if (!file_exists($uri)) {
        $form_state->setErrorByName('managed_file_to_delete', 'Invalid unmanaged file path. Please provide a public file path such as public://file.png');
      }

      $this->unmanagedFileUri = $uri;
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    if (isset($this->loadedEntityReferenceFile) && is_object($this->loadedEntityReferenceFile)) {
      $uri = $this->loadedEntityReferenceFile->getFileUri();
      $this->loadedEntityReferenceFile->delete();
      \Drupal::service('file_system')->delete($uri);
    }

    if (isset($this->unmanagedFileUri) && !empty($this->unmanagedFileUri)) {


      // Check for managed file with uri
      $files = \Drupal::entityTypeManager()->getStorage('file')->loadByProperties(['uri' => $this->unmanagedFileUri]);
      if (!empty($files)) {
        $file = reset($files);
        $file->delete();
      }

      \Drupal::service('file_system')->delete($this->unmanagedFileUri);
    }

    \Drupal::messenger()->addMessage('File(s) deleted successfully.');
  }

}
